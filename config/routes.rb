Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  get 'top/main'
  post 'top/login'
  root 'top#main'
  get 'top/logout'
  get 'top/new'
  post 'top', to: 'top#create'
end
