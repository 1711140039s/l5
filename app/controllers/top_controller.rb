class TopController < ApplicationController
 def main
      if session[:login_uid] != nil
        flash[:notice] = "ログインに成功しました"
        render'main'
      else
        session[:login_uid] = nil
        flash[:notice] = "ログインに失敗しました"
        render'login'
      end
 end
 
 def new
    @user = User.new
 end
 
 def create
    uid= params[:user][:uid]
    pass = params[:user][:pass]
    signup_password = BCrypt::Password.create(pass)
    @user = User.new(uid: uid, pass: signup_password)
    if @user.save
        flash[:notice] = "アカウント作成に成功しました"
      redirect_to '/'
    else
      render 'login'
    end
 end
  
 
 def login

     if user = User.find_by_uid(params[:uid])
             if params[:uid] == user.uid && BCrypt::Password.new(user.pass) == params[:pass]
               session[:login_uid] = user.uid
               redirect_to '/'
             else
               render 'error'
             end
            
     else 
       render 'error'
     end
 end
 
 def logout
  session[:login_uid] = nil
  redirect_to '/'
 end
end
